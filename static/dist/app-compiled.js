/* global XMLHttpRequest */
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

console.log('Start app');
var item = function () {
  console.log('Init module: >items');

  var Item = function Item(name, price) {
    _classCallCheck(this, Item);

    this.name = name;
    this.price = price;
  };

  var Items = function () {
    function Items(response) {
      _classCallCheck(this, Items);

      this.items = JSON.parse(response)['items'];
    }

    _createClass(Items, [{
      key: 'show',
      value: function show() {
        this.items.forEach(function (element) {
          var item = new Item(element['name'], element['price']);
          console.log(item.name);
          console.log(item.price);
        });
      }
    }]);

    return Items;
  }();

  function getJSON(url, callback) {
    var httpReq = new XMLHttpRequest();

    httpReq.onreadystatechange = function () {
      if (httpReq.readyState === XMLHttpRequest.DONE) {
        if (httpReq.status === 200) {
          callback(httpReq.responseText);
        }
      }
    };
    httpReq.open('GET', url);
    httpReq.send();
  }

  getJSON('/items', function (response) {
    var items = new Items(response);
    items.show();
  });
}();

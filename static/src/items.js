/* global XMLHttpRequest */

module.exports = class Items {
  constructor (url) {
    this.url = url
    this.xhr = new XMLHttpRequest()
  }

  json (callback) {
    this.xhr.open('GET', this.url)
    this.xhr.onreadystatechange = () => {
      if (this.xhr.readyState === XMLHttpRequest.DONE) {
        if (this.xhr.status === 200) {
          callback(JSON.parse(this.xhr.responseText))
        }
      }
    }
    this.xhr.send()
  }
}

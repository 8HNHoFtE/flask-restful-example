import os

from flask import Flask
from flask import render_template
from flask_restful import Resource
from flask_restful import Api
from flask_restful import request
from flask_jwt import JWT

from security import authenticate
from security import identity

PORT = int(os.environ.get('PORT', 5000))
HOST = str(os.environ.get('HOST', '0.0.0.0'))
KEY = str(os.environ.get('SECRET_KEY', 'JustForLocalDev'))

app = Flask(__name__)
app.secret_key = KEY
api = Api(app)

jwt = JWT(app, authenticate, identity)

items = []


@app.route('/')
def home():
    return render_template('index.html')


class Item(Resource):

    def get(self, name):
        item = next(filter(lambda x: x['name'] == name, items), None)
        if item:
            return {'item': item}, 200
        else:
            return {'item': 'Not found'}, 404

    def post(self, name):
        item = next(filter(lambda x: x['name'] == name, items), None)
        if item:
            message = "An item with name '{}' already exist.".format(name)
            return {'message': message}, 400
        else:
            data = request.get_json()
            item = {'name': name, 'price': data['price']}
            items.append(item)
            return item, 201


class ItemList(Resource):

    def get(self):
        if items:
            return {'items': items}
        else:
            return {'items': 'Not found'}, 404


api.add_resource(Item, '/item/<string:name>')
api.add_resource(ItemList, '/items')

app.run(host=HOST, port=PORT, debug=True)
